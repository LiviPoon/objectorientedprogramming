ArrayList<Circle> gameObjects = new ArrayList<Circle>();

void addCircle() {
    /* Add a new circle where the user clicked. */
    
    gameObjects.add(new Circle(mouseX, mouseY));
}

void reset() {
    /* Clear all game objects. */

    gameObjects.clear();
}

void setup() {
    size(400, 400);
}

void draw() {
    /* 
     * Clear the screen, have all game objects
     * update and redraw.
     */

    // draw a white background
    background(255, 255, 255);
    
    for (Circle gameObject : gameObjects) {
        gameObject.update();
        gameObject.draw();
    }
}

void mouseClicked() {
    addCircle();
}

void keyPressed() {
    if (key == 'r') {
        reset();
    }
}
