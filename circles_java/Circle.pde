class Circle {
    float x;
    float y;
    float xSpeed;
    float ySpeed;
    color col;
    float size;
    
    Circle(float x, float y) {
        /* 
         * Create a new circle at the given 
         * x,y point with a random speed,
         * color, and size.
         */

        this.x = x;
        this.y = y;
        this.xSpeed = random(-5,5);
        this.ySpeed = random(-5,5);
        this.col = color(random(0,255),
                         random(0,255),
                         random(0,255));
        this.size = random(5,75);
  }

    void update() {
        /* Update current location by speed. */

        this.x += this.xSpeed;
        this.y += this.ySpeed;
        
        if (this.x > 400) {
          this.xSpeed = -this.xSpeed; 
        }
        
        if (this.x < 0 ) {
          this.xSpeed = -this.xSpeed;
        }
        
        if (this.y > 400) {
          this.ySpeed = -this.ySpeed; 
        }
        
        if (this.y < 0) {
          this.ySpeed = -this.ySpeed;
        }
    }

    void draw() {
        /* Draw self on the canvas. */

        fill(this.col); // set shape color
        stroke(0,0,0); // add a black outline
        ellipse(this.x, this.y, this.size, this.size);
    }
}
