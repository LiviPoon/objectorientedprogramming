class Bird extends PhysicalObject{
  float minYSpeed = -1.0;
  float maxYSpeed = 1.0;
  float minXSpeed = 2.0;
  float maxXSpeed = 5.0;
  float size = 15.0;
  color fillColor;
  
  Bird(float x, float y, float size) {
    super(x, y, size);
    this.xSpeed = random(this.minXSpeed,this.maxXSpeed);
    this.ySpeed = random(this.minYSpeed, this.maxYSpeed);
  }
  
  void display() {
    ellipse(this.x, this.y, this.size*2, this.size);
  }
  
  void move() {
    this.x += this.xSpeed;
    this.y += this.ySpeed;
    // if off the right side of the screen
    // move to just off the left side of the screen
    if (this.x > width) {
      this.x = -this.size;
    }
    // if off the top or bottom of the screen, reverse y speed
    if (this.y < -this.size || this.y > height) {
      this.ySpeed = -this.ySpeed;
    }
    // about 20% of the time, change speed a bit
    if (random(0,1) > 0.8) {
      this.ySpeed += random(-0.5, 0.5);
      this.ySpeed = max(this.minYSpeed, this.ySpeed);
      this.ySpeed = min(this.maxYSpeed, this.ySpeed);

      this.xSpeed += random(-0.5, 0.5);
      this.xSpeed = max(this.minXSpeed, this.xSpeed);
      this.xSpeed = min(this.maxXSpeed, this.xSpeed);
    }
  }
  
}
