class PhysicalObject {
  float x;
  float y;
  float xSpeed;
  float ySpeed;
  float size;
  color fillColor;
  
  PhysicalObject (float x, float y, float size) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.xSpeed = random(-1,1);
    this.ySpeed = random(0.5, 1.5);
    this.fillColor = color(random(0,255), random(0,255), random(0,255));
  }
  
  void display() {
    fill(this.fillColor);
  }
  
  void move() {
    this.x += this.xSpeed;
    this.y += this.ySpeed;
    if (this.y > height) {
      this.y = -this.size;
    }
    // about 5% of the time,
    // or if going off the left or right of the screen,
    // reverse x direction
    if (random(0, 1) > 0.95 || this.x < -this.size || this.x > width) {
      this.xSpeed = -this.xSpeed;
    }
  }
  
}
