class FlittingBird extends Bird {

  FlittingBird(float x, float y, float size) {
    super(x, y, size);
  }

  void display() {
    super.display();
  }
  
  void move() {
    super.move();
  }
}
