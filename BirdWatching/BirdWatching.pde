ArrayList<SoaringBird> SOARINGBIRDS = new ArrayList<SoaringBird>();
ArrayList<FallingFeather> FALLINGFEATHERS = new ArrayList<FallingFeather>();
ArrayList<FlittingBird> FLITTINGBIRDS = new ArrayList<FlittingBird>();

void setup() {
  noStroke();
  size(600, 400);
  
  for (int i = 0; i < 5; i++) {
  FLITTINGBIRDS.add(new FlittingBird(random(100), random(100), 15.0));
  FALLINGFEATHERS.add(new FallingFeather());
  SOARINGBIRDS.add(new SoaringBird(random(100), random(100), 15.0));
}

}

void draw() {
  background(173,216,230);
  
  for ( SoaringBird i: SOARINGBIRDS) {
    i.move();
    i.display();
  }

  for ( FlittingBird i : FLITTINGBIRDS) {
    i.move();
    i.display();
  }
  
  for( FallingFeather i : FALLINGFEATHERS) {
    i.move();
    i.display();
  }
}
