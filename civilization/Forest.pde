//this is just a subclass of terrain, and doesn't make any changes

class Forest extends Terrain {
  //declare class
  Forest(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, color terrainColor){
    //inherit super class
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }
}
