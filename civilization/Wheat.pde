class Wheat extends Terrain {
  //Inherit Super Class
  Wheat(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, color terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }
}
