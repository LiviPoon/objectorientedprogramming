class Terrain {
  public float terrainPositionX;
  public float terrainPositionY;
  public float terrainGrowthRate;
  public color terrainColor;
  public int counter;
  public String statechange = "up";
  public boolean expandableTerrain;

  Terrain(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, color terrainColor) {
    this.expandableTerrain = expandableTerrain;
    this.terrainColor = terrainColor;
    this.terrainPositionX = terrainPositionX;
    this.terrainPositionY = terrainPositionY;
    this.terrainGrowthRate = terrainGrowthRate;
  }

  public void display(){
    fill(this.terrainColor);
    circle(this.terrainPositionX,this.terrainPositionY,10);

  };

  public void checkTerrainGrowthCoordinate(int[][] map, int terrainID){
    //Move depending on what state the instance is in, change once certain criteria is met.
    if(statechange == "up"){
      if (map[int(this.terrainPositionX/10+ 1)][int(this.terrainPositionY/10)] != terrainID){
        statechange = "right";
        this.terrainPositionX += 10;
      }
      else {
          this.terrainPositionY += 10;
      }
    }

    else if(statechange == "right"){
      if (map[int(this.terrainPositionX/10)][int((this.terrainPositionY/10) - 1)] != terrainID){
        statechange = "down";
        this.terrainPositionY -= 10;
      }
      else{
        this.terrainPositionX += 10;
      }
    }

   else if(statechange == "down") {
      if (map[int(this.terrainPositionX/10 - 1)][int(this.terrainPositionY/10)] != terrainID){
       statechange = "left";
       this.terrainPositionX -= 10;
     }
     else {
       this.terrainPositionY -= 10;
     }
    }

   else if(statechange == "left"){
      if (map [int(this.terrainPositionX/10)][int(this.terrainPositionY/10 + 1)] != terrainID){
        statechange = "up";
        this.terrainPositionY += 10;
    }
      else{
        this.terrainPositionX -= 10;
      }
  }

}

  public void checkIfInBounds(){
    //Check if the terrain is inside the screen
    if (int(this.terrainPositionX)/10 <= 1){
      this.terrainPositionX += 10;
    }

    if (int(this.terrainPositionY)/10 <= 1) {
       this.terrainPositionY += 10;
    }

    if (int(this.terrainPositionX)/10 >= 100) {
       this.terrainPositionX -= 10;
    }

    if (int(this.terrainPositionY)/10 >= 60) {
       this.terrainPositionY -= 10;
    }
  }

  public void growTerrain(int[][] map, int terrainID){
          //grow the terrain with certain color size and make sure that its in the screen
          fill(this.terrainColor);
          map[int(this.terrainPositionX/10)][int(this.terrainPositionY/10)] = terrainID;
          circle(this.terrainPositionX, this.terrainPositionY, 10);
          checkTerrainGrowthCoordinate(map, terrainID);
          checkIfInBounds();
  }

  public void simulate(int[][] map, int terrainID){
    //counter updates terrain position depending on growth rate.
    if (this.counter >= this.terrainGrowthRate){
      growTerrain(map, terrainID);
      this.counter = 0;
    }
    else {
      this.counter++;
    }


  }
}
