class RandomTerrain extends Terrain {
  public int[] positionTerrainChangeArray; //declare random position variables
  private int randomxValue;
  private int randomyValue;

  //inherit terrian class
  RandomTerrain (float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, color terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }

  @Override //overide superclass code, and rewrite it

  //method for checking the growth of the randomTerrain to make sure that it grows within the window and the right locations.
  void checkTerrainGrowthCoordinate(int[][] map, int terrainID){
    positionTerrainChangeArray = new int[3]; //define length of random values
    positionTerrainChangeArray [0] = -10; //define random values
    positionTerrainChangeArray [1] = 10;
    positionTerrainChangeArray [2] = 0;
    int rnd = new Random().nextInt(positionTerrainChangeArray.length);
    int rnd2 = new Random().nextInt(positionTerrainChangeArray.length);
    randomxValue = positionTerrainChangeArray[rnd]; //get random values
    randomyValue = positionTerrainChangeArray[rnd2];

    //if you add the random value, does it go outside the canvas? Make sure that it doesn't!
    if (int(this.terrainPositionX + rnd)/10 <= 1){
      this.terrainPositionX += 10;
    }

    if (int(this.terrainPositionY + rnd2)/10 <= 1) {
       this.terrainPositionY += 10;
    }

    if (int(this.terrainPositionX + rnd )/10 >= 100) {
       this.terrainPositionX -= 10;
    }

    if (int(this.terrainPositionY + rnd2)/10 >= 60) {
       this.terrainPositionY -= 10;
    }
  }
  //end of boundry check

  void growTerrain(int[][] map, int terrainID) { //main function for handling growth
    fill(this.terrainColor);
    map[int(this.terrainPositionX/10)][int(this.terrainPositionY/10)] = terrainID;
    circle(this.terrainPositionX, this.terrainPositionY, 10);
    checkTerrainGrowthCoordinate(map, terrainID);

    //grow the terrain
    if (map[int(this.terrainPositionX + randomxValue)/10][int(this.terrainPositionY + randomyValue)/10] == 0){
      this.terrainPositionX = this.terrainPositionX +  randomxValue;
      this.terrainPositionY = this.terrainPositionY + randomyValue;
    }


  }

  void simulate(int[][] map, int terrainID){ //function that handles rate of growth
    if (this.counter >= this.terrainGrowthRate){
      growTerrain(map, terrainID);
      this.counter = 0;
    }
    else {
      this.counter++;
    }
  }

}
