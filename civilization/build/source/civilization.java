import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Random; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class civilization extends PApplet {

//civilization - a simple country simulator

/*
The goal of this project was focused on allowing me to understand agent based
modeling at a surface level, while also implementing a more complex OOP
structure within the project. I also have been fasinated by simulations that
simulate the rise and downfall of species and civilizations. Two of my inspirations
for this project was the game Civilization V and VI, and also various simulations used by
biologists today.
*/


/*
civilization - main file
├── countryINFO
├── country
└── terrain
    ├── forest
    ├── wheat
    └── randomTerrain
        └── rock
*/


//declare array lists for every class where ther are muitiple instances
ArrayList<Country> COUNTRIES = new ArrayList <Country>();
ArrayList<Wheat> WHEAT = new ArrayList <Wheat>();
ArrayList<Forest> FOREST = new ArrayList <Forest>();
ArrayList<Rock> ROCK = new ArrayList <Rock>();
ArrayList<countryINFO> INFO = new ArrayList <countryINFO>();

//declare map
private int[][] map = new int[100][60];

//declare the amount of each class you want on the canvas
private int numCountries = 3;
private int numWheat = PApplet.parseInt(random(4));
private int numForest = PApplet.parseInt(random(4));
private int numRock = PApplet.parseInt(1);
private int spacing = 0;

//declare special ids for terrain, make sure that these ids won't overlap with countryIDs and other terrainIDs
public int terrainWheatID = 69;
public int terrainForestID = 70;
public int terrainRockID = 71;

public void setup() {
  noStroke();
  

  //take the length of numCounties and create that many new country instances
  for (int i = 0; i < numCountries; i++) {
    float randomxValue = PApplet.parseInt(random(8))*100;
    float randomyValue = PApplet.parseInt(random(4))*100;
    COUNTRIES.add(new Country(randomxValue, randomyValue,"blue", color(random(200),random(200), random(200))));
    INFO.add(new countryINFO(COUNTRIES.get(i),spacing));
    spacing += 100;
  }

  //take the length of numWheat and create that many new wheat instances
  for (int i = 0; i < numWheat; i++) {
    float wheatRandomxValue = PApplet.parseInt(random(8))*100;
    float wheatRandomyValue = PApplet.parseInt(random(4))*100;
    WHEAT.add(new Wheat(wheatRandomxValue,wheatRandomyValue,true, 1, color(249,194,46)));
  }

  //take the length of numForest and create that many new forest instances
  for (int i = 0; i < numForest; i++) {
    float forestRandomxValue = PApplet.parseInt(random(8))*100;
    float forestRandomyValue = PApplet.parseInt(random(4))*100;
    FOREST.add(new Forest(forestRandomxValue,forestRandomyValue,true, 10, color(36,83,54)));
  }

  //take the length of numRock and create that many new rock instances
  for (int i = 0; i < numRock; i++) {
    float rockRandomxValue = PApplet.parseInt(random(8))*100;
    float rockRandomyValue = PApplet.parseInt(random(4))*100;
      ROCK.add(new Rock(rockRandomxValue,rockRandomyValue,true, 10, color(61,90,108)));
  }
}

public void draw() {
  //write descriptions/legend
  stroke(0);
  fill(200);
  rect(1010, 10, 280, 590);
  fill(0);
  text("civilization simulation", 1050, 50);
  text("By Livi Poon", 1050, 75);

  text("Legend", 1050, 150);
  // text("Note High Growth Rate Is Sec Before Growth. Growth is how lon")

  text("Forests: -growth -food", 1050, 175);
  fill(36,83,54);
  circle(1035, 170, 10);

  fill(0);
  text("Wheat Fields: +growth +food", 1050, 200);
  fill(249,194,46);
  circle(1035, 195, 10);

  fill(0);
  text("Rocks: non-traversable", 1050, 225);
  fill(61,90,108);
  circle(1035, 220, 10);

  fill(0);
  text("Civilizations", 1050, 275);

  //end of descriptions/legend

  noStroke();
  int countryID = 0; //Declare integer counter for country ID
  for (Country i: COUNTRIES) { //i changes depending on the amount of countries and terrain needed.
    countryID += 1; //each different country has it's own ID
    i.display(); //display the country on the map
    i.simulate(this.map, countryID, terrainWheatID, terrainForestID);//update it's location
  }

  for (Wheat i: WHEAT){ //for each instance of Wheat, update its location and where it is.
    i.display();
    i.simulate(this.map, terrainWheatID);
  }

  for (Forest i: FOREST){ //for each instance of Forest, update its location and where it is.
    i.display();
    i.simulate(this.map, terrainForestID);
  }

  for (Rock i: ROCK){ //for each instance of Rock, update its location and where it is.
    i.display();
    i.simulate(this.map, terrainRockID);
  }

  for (countryINFO i: INFO){ //for each instance of Rock, update its location and where it is.
    i.display();
  }
}
 //import internal libraries

class Country {
  private float positionX; //declare postioning
  private float positionY;

 //declare country specific properties
  private int dmg;
  private int dmgDealt;
  private int dmgTaken;
  private int flagColor;
  private String nation;
  private int growthRate;
  private int troops = PApplet.parseInt(random(10000));
  private int population = PApplet.parseInt(random(10000));
  private int food = PApplet.parseInt(random(100000));

  //declare postion variables
  private int  randomPositionChange;
  private int  randomPositionChange2;
  private int[] positionChangeArray;

  private int counter;

  //declare global map
  private int[] map;

  Country(float positionX, float positionY, String nation, int flagColor){
    //set constructors to the given variables

    this.positionX = positionX; //set values for position
    this.positionY = positionY;

    this.nation = nation; //set the name of the nation
    this.flagColor = flagColor; //set the color it will be on the canvas

    this.positionChangeArray = new int[3]; //create an array for random movement values
    this.counter = 0; //define counter for growth use
  }

 public void checkCoordinateChange(){

    //define random variables - What do the numbers mean?
    this.positionChangeArray [0] = -10;
    this.positionChangeArray [1] = 10;
    this.positionChangeArray [2] = 0;

    //INSERT COMMENT
    int rnd = new Random().nextInt(positionChangeArray.length);
    int rnd2 = new Random().nextInt(positionChangeArray.length);
    randomPositionChange = positionChangeArray[rnd];
    randomPositionChange2 = positionChangeArray[rnd2];

    //check if random movement is in bounds of the screen
    if (PApplet.parseInt(this.positionX + randomPositionChange)/10 <= 1){
      this.positionX += 10;
    }

    if (PApplet.parseInt(this.positionY + randomPositionChange2)/10 <= 1) {
       this.positionY += 10;
    }

    if (PApplet.parseInt(this.positionX + randomPositionChange )/10 >= 100) {
       this.positionX -= 10;
    }

    if (PApplet.parseInt(this.positionY + randomPositionChange2)/10 >= 60) {
       this.positionY -= 10;
    }


 }
 public void expandCountry(int[][] map, int countryID, int terrainWheatID, int terrainForestID) {
          //this function is being used as a main function for country movement

          noStroke();
          fill(this.flagColor); //leave a mark where the country is currently
          circle(this.positionX, this.positionY, 10);

          checkCoordinateChange(); //check and move the country

          map[PApplet.parseInt(this.positionX/10)][PApplet.parseInt(this.positionY/10)] = countryID; //update map with new location
          troops += 1000; //update countries stats
          population += 500;

          //determine if the new location is open for movement or not, if not then generate a new place to move

          if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == 0){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == countryID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == terrainWheatID){ //if the country goes on a wheat tile increase food consumption
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
            food += PApplet.parseInt(random(1000));
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == terrainForestID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
            food -= PApplet.parseInt(random(1000));
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] != countryID){
            if (troops <= 1000){
              checkCoordinateChange();
            }

            else{
              this.positionX = this.positionX + randomPositionChange;
              this.positionY = this.positionY + randomPositionChange2;
            }

          }
 }

  public void display(){
    noStroke();
    fill(this.flagColor);
    circle(this.positionX, this.positionY, 10);
  }

  public void simulate(int[][] map, int countryID, int terrainWheatID, int terrainForestID){
    //define growth rate, and change based on amount of food
    if (population - food >= 10) {
      this.growthRate = 10;
    }

    //depending on the amount of population and food change the growth rate
    else if (population - food <= 0){
      if (food < 0){
        population -= 5000;
      }

      if (population < 0){
        this.growthRate = 1000;
      }
    }

    else{
      this.growthRate = population - food;

    }

    //if the counter is greater than the growthRate then the country grows if not then it countinues with the loop
    if (this.counter >= this.growthRate){
      expandCountry(map, countryID, terrainWheatID, terrainForestID);

      population += random(100);
      food -= random(100);
      this.counter = 0;
    }

    else {
      this.counter++;
      food -= random(100);

    }
  }
}
//this is just a subclass of terrain, and doesn't make any changes

class Forest extends Terrain {
  //declare class
  Forest(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor){
    //inherit super class
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }
}
class RandomTerrain extends Terrain {
  public int[] positionTerrainChangeArray; //declare random position variables
  private int randomxValue;
  private int randomyValue;

  //inherit terrian class
  RandomTerrain (float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }

  public @Override //overide superclass code, and rewrite it

  //method for checking the growth of the randomTerrain to make sure that it grows within the window and the right locations.
  void checkTerrainGrowthCoordinate(int[][] map, int terrainID){
    positionTerrainChangeArray = new int[3]; //define length of random values
    positionTerrainChangeArray [0] = -10; //define random values
    positionTerrainChangeArray [1] = 10;
    positionTerrainChangeArray [2] = 0;
    int rnd = new Random().nextInt(positionTerrainChangeArray.length);
    int rnd2 = new Random().nextInt(positionTerrainChangeArray.length);
    randomxValue = positionTerrainChangeArray[rnd]; //get random values
    randomyValue = positionTerrainChangeArray[rnd2];

    //if you add the random value, does it go outside the canvas? Make sure that it doesn't!
    if (PApplet.parseInt(this.terrainPositionX + rnd)/10 <= 1){
      this.terrainPositionX += 10;
    }

    if (PApplet.parseInt(this.terrainPositionY + rnd2)/10 <= 1) {
       this.terrainPositionY += 10;
    }

    if (PApplet.parseInt(this.terrainPositionX + rnd )/10 >= 100) {
       this.terrainPositionX -= 10;
    }

    if (PApplet.parseInt(this.terrainPositionY + rnd2)/10 >= 60) {
       this.terrainPositionY -= 10;
    }
  }
  //end of boundry check

  public void growTerrain(int[][] map, int terrainID) { //main function for handling growth
    fill(this.terrainColor);
    map[PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt(this.terrainPositionY/10)] = terrainID;
    circle(this.terrainPositionX, this.terrainPositionY, 10);
    checkTerrainGrowthCoordinate(map, terrainID);

    //grow the terrain
    if (map[PApplet.parseInt(this.terrainPositionX + randomxValue)/10][PApplet.parseInt(this.terrainPositionY + randomyValue)/10] == 0){
      this.terrainPositionX = this.terrainPositionX +  randomxValue;
      this.terrainPositionY = this.terrainPositionY + randomyValue;
    }


  }

  public void simulate(int[][] map, int terrainID){ //function that handles rate of growth
    if (this.counter >= this.terrainGrowthRate){
      growTerrain(map, terrainID);
      this.counter = 0;
    }
    else {
      this.counter++;
    }
  }

}
class Rock extends RandomTerrain {
  //inherit RandomTerrain
  Rock(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }

}
class Terrain {
  public float terrainPositionX;
  public float terrainPositionY;
  public float terrainGrowthRate;
  public int terrainColor;
  public int counter;
  public String statechange = "up";
  public boolean expandableTerrain;

  Terrain(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor) {
    this.expandableTerrain = expandableTerrain;
    this.terrainColor = terrainColor;
    this.terrainPositionX = terrainPositionX;
    this.terrainPositionY = terrainPositionY;
    this.terrainGrowthRate = terrainGrowthRate;
  }

  public void display(){
    fill(this.terrainColor);
    circle(this.terrainPositionX,this.terrainPositionY,10);

  };

  public void checkTerrainGrowthCoordinate(int[][] map, int terrainID){
    //Move depending on what state the instance is in, change once certain criteria is met.
    if(statechange == "up"){
      if (map[PApplet.parseInt(this.terrainPositionX/10+ 1)][PApplet.parseInt(this.terrainPositionY/10)] != terrainID){
        statechange = "right";
        this.terrainPositionX += 10;
      }
      else {
          this.terrainPositionY += 10;
      }
    }

    else if(statechange == "right"){
      if (map[PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt((this.terrainPositionY/10) - 1)] != terrainID){
        statechange = "down";
        this.terrainPositionY -= 10;
      }
      else{
        this.terrainPositionX += 10;
      }
    }

   else if(statechange == "down") {
      if (map[PApplet.parseInt(this.terrainPositionX/10 - 1)][PApplet.parseInt(this.terrainPositionY/10)] != terrainID){
       statechange = "left";
       this.terrainPositionX -= 10;
     }
     else {
       this.terrainPositionY -= 10;
     }
    }

   else if(statechange == "left"){
      if (map [PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt(this.terrainPositionY/10 + 1)] != terrainID){
        statechange = "up";
        this.terrainPositionY += 10;
    }
      else{
        this.terrainPositionX -= 10;
      }
  }

}

  public void checkIfInBounds(){
    //Check if the terrain is inside the screen
    if (PApplet.parseInt(this.terrainPositionX)/10 <= 1){
      this.terrainPositionX += 10;
    }

    if (PApplet.parseInt(this.terrainPositionY)/10 <= 1) {
       this.terrainPositionY += 10;
    }

    if (PApplet.parseInt(this.terrainPositionX)/10 >= 100) {
       this.terrainPositionX -= 10;
    }

    if (PApplet.parseInt(this.terrainPositionY)/10 >= 60) {
       this.terrainPositionY -= 10;
    }
  }

  public void growTerrain(int[][] map, int terrainID){
          //grow the terrain with certain color size and make sure that its in the screen
          fill(this.terrainColor);
          map[PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt(this.terrainPositionY/10)] = terrainID;
          circle(this.terrainPositionX, this.terrainPositionY, 10);
          checkTerrainGrowthCoordinate(map, terrainID);
          checkIfInBounds();
  }

  public void simulate(int[][] map, int terrainID){
    //counter updates terrain position depending on growth rate.
    if (this.counter >= this.terrainGrowthRate){
      growTerrain(map, terrainID);
      this.counter = 0;
    }
    else {
      this.counter++;
    }


  }
}
class Wheat extends Terrain {
  //Inherit Super Class
  Wheat(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }
}
class countryINFO {
  private Country selectedCountry;
  private int spacing;

  countryINFO(Country country, int spacing){
    this.selectedCountry = country; //set this.selectedCountry to the given Country Instance.
    this.spacing = spacing;
  }

  private void display(){
    fill(0);
    //display country data
    text(this.selectedCountry.flagColor + " Population: " + this.selectedCountry.population, 1050, 310 + this.spacing);
    text(this.selectedCountry.flagColor + " Food: " + this.selectedCountry.food, 1050, 330 + this.spacing);
    text(this.selectedCountry.flagColor + " Troops: " + this.selectedCountry.troops, 1050, 350 + this.spacing);
    text(this.selectedCountry.flagColor + " Growth Rate: " + this.selectedCountry.growthRate, 1050, 370 + this.spacing);

  }
}
  public void settings() {  size(1300, 610); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "civilization" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
