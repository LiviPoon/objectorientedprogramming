import java.util.Random; //import internal libraries

class Country {
  private float positionX; //declare postioning
  private float positionY;

 //declare country specific properties
  private int dmg;
  private int dmgDealt;
  private int dmgTaken;
  private color flagColor;
  private String nation;
  private int growthRate;
  private int troops = int(random(10000));
  private int population = int(random(10000));
  private int food = int(random(100000));

  //declare postion variables
  private int  randomPositionChange;
  private int  randomPositionChange2;
  private int[] positionChangeArray;

  private int counter;

  //declare global map
  private int[] map;

  Country(float positionX, float positionY, String nation, color flagColor){
    //set constructors to the given variables

    this.positionX = positionX; //set values for position
    this.positionY = positionY;

    this.nation = nation; //set the name of the nation
    this.flagColor = flagColor; //set the color it will be on the canvas

    this.positionChangeArray = new int[3]; //create an array for random movement values
    this.counter = 0; //define counter for growth use
  }

 void checkCoordinateChange(){

    //define random variables - What do the numbers mean?
    this.positionChangeArray [0] = -10;
    this.positionChangeArray [1] = 10;
    this.positionChangeArray [2] = 0;

    //INSERT COMMENT
    int rnd = new Random().nextInt(positionChangeArray.length);
    int rnd2 = new Random().nextInt(positionChangeArray.length);
    randomPositionChange = positionChangeArray[rnd];
    randomPositionChange2 = positionChangeArray[rnd2];

    //check if random movement is in bounds of the screen
    if (int(this.positionX + randomPositionChange)/10 <= 1){
      this.positionX += 10;
    }

    if (int(this.positionY + randomPositionChange2)/10 <= 1) {
       this.positionY += 10;
    }

    if (int(this.positionX + randomPositionChange )/10 >= 100) {
       this.positionX -= 10;
    }

    if (int(this.positionY + randomPositionChange2)/10 >= 60) {
       this.positionY -= 10;
    }


 }
 public void expandCountry(int[][] map, int countryID, int terrainWheatID, int terrainForestID) {
          //this function is being used as a main function for country movement

          noStroke();
          fill(this.flagColor); //leave a mark where the country is currently
          circle(this.positionX, this.positionY, 10);

          checkCoordinateChange(); //check and move the country

          map[int(this.positionX/10)][int(this.positionY/10)] = countryID; //update map with new location
          troops += 1000; //update countries stats
          population += 500;

          //determine if the new location is open for movement or not, if not then generate a new place to move

          if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == 0){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == countryID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == terrainWheatID){ //if the country goes on a wheat tile increase food consumption
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
            food += int(random(1000));
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == terrainForestID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
            food -= int(random(1000));
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] != countryID){
            if (troops <= 1000){
              checkCoordinateChange();
            }

            else{
              this.positionX = this.positionX + randomPositionChange;
              this.positionY = this.positionY + randomPositionChange2;
            }

          }
 }

  void display(){
    noStroke();
    fill(this.flagColor);
    circle(this.positionX, this.positionY, 10);
  }

  void simulate(int[][] map, int countryID, int terrainWheatID, int terrainForestID){
    //define growth rate, and change based on amount of food
    if (population - food >= 10) {
      this.growthRate = 10;
    }

    //depending on the amount of population and food change the growth rate
    else if (population - food <= 0){
      if (food < 0){
        population -= 5000;
      }

      if (population < 0){
        this.growthRate = 1000;
      }
    }

    else{
      this.growthRate = population - food;

    }

    //if the counter is greater than the growthRate then the country grows if not then it countinues with the loop
    if (this.counter >= this.growthRate){
      expandCountry(map, countryID, terrainWheatID, terrainForestID);

      population += random(100);
      food -= random(100);
      this.counter = 0;
    }

    else {
      this.counter++;
      food -= random(100);

    }
  }
}
