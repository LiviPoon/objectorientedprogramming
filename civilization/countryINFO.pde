class countryINFO {
  private Country selectedCountry;
  private int spacing;

  countryINFO(Country country, int spacing){
    this.selectedCountry = country; //set this.selectedCountry to the given Country Instance.
    this.spacing = spacing;
  }

  private void display(){
    fill(0);
    //display country data
    text(this.selectedCountry.flagColor + " Population: " + this.selectedCountry.population, 1050, 310 + this.spacing);
    text(this.selectedCountry.flagColor + " Food: " + this.selectedCountry.food, 1050, 330 + this.spacing);
    text(this.selectedCountry.flagColor + " Troops: " + this.selectedCountry.troops, 1050, 350 + this.spacing);
    text(this.selectedCountry.flagColor + " Growth Rate: " + this.selectedCountry.growthRate, 1050, 370 + this.spacing);

  }
}
