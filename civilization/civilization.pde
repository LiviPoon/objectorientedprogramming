//civilization - a simple country simulator

/*
The goal of this project was focused on allowing me to understand agent based
modeling at a surface level, while also implementing a more complex OOP
structure within the project. I also have been fasinated by simulations that
simulate the rise and downfall of species and civilizations. Two of my inspirations
for this project was the game Civilization V and VI, and also various simulations used by
biologists today.
*/


/*
civilization - main file
├── countryINFO
├── country
└── terrain
    ├── forest
    ├── wheat
    └── randomTerrain
        └── rock
*/


//declare array lists for every class where ther are muitiple instances
ArrayList<Country> COUNTRIES = new ArrayList <Country>();
ArrayList<Wheat> WHEAT = new ArrayList <Wheat>();
ArrayList<Forest> FOREST = new ArrayList <Forest>();
ArrayList<Rock> ROCK = new ArrayList <Rock>();
ArrayList<countryINFO> INFO = new ArrayList <countryINFO>();

//declare map
private int[][] map = new int[100][60];

//declare the amount of each class you want on the canvas
private int numCountries = 3;
private int numWheat = int(random(4));
private int numForest = int(random(4));
private int numRock = int(1);
private int spacing = 0;

//declare special ids for terrain, make sure that these ids won't overlap with countryIDs and other terrainIDs
public int terrainWheatID = 69;
public int terrainForestID = 70;
public int terrainRockID = 71;

void setup() {
  noStroke();
  size(1300, 610);

  //take the length of numCounties and create that many new country instances
  for (int i = 0; i < numCountries; i++) {
    float randomxValue = int(random(8))*100;
    float randomyValue = int(random(4))*100;
    COUNTRIES.add(new Country(randomxValue, randomyValue,"blue", color(random(200),random(200), random(200))));
    INFO.add(new countryINFO(COUNTRIES.get(i),spacing));
    spacing += 100;
  }

  //take the length of numWheat and create that many new wheat instances
  for (int i = 0; i < numWheat; i++) {
    float wheatRandomxValue = int(random(8))*100;
    float wheatRandomyValue = int(random(4))*100;
    WHEAT.add(new Wheat(wheatRandomxValue,wheatRandomyValue,true, 1, color(249,194,46)));
  }

  //take the length of numForest and create that many new forest instances
  for (int i = 0; i < numForest; i++) {
    float forestRandomxValue = int(random(8))*100;
    float forestRandomyValue = int(random(4))*100;
    FOREST.add(new Forest(forestRandomxValue,forestRandomyValue,true, 10, color(36,83,54)));
  }

  //take the length of numRock and create that many new rock instances
  for (int i = 0; i < numRock; i++) {
    float rockRandomxValue = int(random(8))*100;
    float rockRandomyValue = int(random(4))*100;
      ROCK.add(new Rock(rockRandomxValue,rockRandomyValue,true, 10, color(61,90,108)));
  }
}

void draw() {
  //write descriptions/legend
  stroke(0);
  fill(200);
  rect(1010, 10, 280, 590);
  fill(0);
  text("civilization simulation", 1050, 50);
  text("By Livi Poon", 1050, 75);

  text("Legend", 1050, 150);
  // text("Note High Growth Rate Is Sec Before Growth. Growth is how lon")

  text("Forests: -growth -food", 1050, 175);
  fill(36,83,54);
  circle(1035, 170, 10);

  fill(0);
  text("Wheat Fields: +growth +food", 1050, 200);
  fill(249,194,46);
  circle(1035, 195, 10);

  fill(0);
  text("Rocks: non-traversable", 1050, 225);
  fill(61,90,108);
  circle(1035, 220, 10);

  fill(0);
  text("Civilizations", 1050, 275);

  //end of descriptions/legend

  noStroke();
  int countryID = 0; //Declare integer counter for country ID
  for (Country i: COUNTRIES) { //i changes depending on the amount of countries and terrain needed.
    countryID += 1; //each different country has it's own ID
    i.display(); //display the country on the map
    i.simulate(this.map, countryID, terrainWheatID, terrainForestID);//update it's location
  }

  for (Wheat i: WHEAT){ //for each instance of Wheat, update its location and where it is.
    i.display();
    i.simulate(this.map, terrainWheatID);
  }

  for (Forest i: FOREST){ //for each instance of Forest, update its location and where it is.
    i.display();
    i.simulate(this.map, terrainForestID);
  }

  for (Rock i: ROCK){ //for each instance of Rock, update its location and where it is.
    i.display();
    i.simulate(this.map, terrainRockID);
  }

  for (countryINFO i: INFO){ //for each instance of Rock, update its location and where it is.
    i.display();
  }
}
