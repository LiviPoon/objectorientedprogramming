import java.util.Scanner;

class DiceGame {
    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("How many sides should the dice have?");
        int sides = scanner.nextInt();

        System.out.println("How many dice should roll?");
        int numDice = scanner.nextInt();
    
        Dice myDice = new Dice(sides, numDice);
        Dice yourDice = new Dice(sides, numDice);

        boolean keepPlaying = true;
        while (keepPlaying) {

            System.out.println("Rolling the dice...");
            Thread.sleep(500); // pause for half a minute
            myDice.compete(yourDice);

            System.out.println("Do you want to play again? y/n");
            String response = scanner.next();
            if (!response.equals("y")) {
                keepPlaying = false;
            }
        }

        System.out.println("Thank you for playing!");
    }
}
