import java.util.Random;
import java.util.ArrayList;
import java.lang.Integer;

class Dice {
    int sides;                 // how many sides each die has
    int numDice;               // how many dice in the set
    Random random;             // random number generator
    ArrayList<Integer> values; // current values of the dice
    int score;                 // current score of the dice

    Dice(int sides, int numDice) {
        this.sides = sides;
        this.numDice = numDice;
        this.random = new Random();
        this.values = new ArrayList<Integer>();
        this.score = 0;
    }

    void roll() {
        /* get new values for each die and update the score */
        ArrayList<Integer> values = new ArrayList<Integer>();
        for (int index = 0; index < this.numDice; ++index) {
            // nextInt returns a random value in [0,sides), so add 1 to them
            values.add(this.random.nextInt(this.sides) + 1);
        }
        this.values = values;
        this.computeScore();
    }

    void computeScore() {
        /* compute and save the score of these dice */
        int score = 0;
        for (Integer value : this.values) {
            score += value;
        }
        this.score = score;
    }

    String diceDisplay() {
        /* print out the value of each dice and the overall score */

        StringBuilder diceString = new StringBuilder();
        for (Integer value: this.values) {
            diceString.append(value);
            diceString.append(" ");
        }

        return diceString.toString() + ": " + this.score;
    }

    void compete(Dice otherDice) {
        /* Roll each set of dice. Print out the results. */

        this.roll();
        System.out.println("I rolled " + this.diceDisplay());

        otherDice.roll();
        System.out.println("You rolled " + otherDice.diceDisplay());

        if (otherDice.score == this.score) {
            System.out.println("Tie!");
        }
        else if (this.score > otherDice.score) {
            System.out.println("COMPUTERS WIN!");
        }
        else {
            System.out.println("HUMANS WIN4!");
        }
    }
}
