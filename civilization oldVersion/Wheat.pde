class Wheat extends Terrain {
  
  Wheat(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, color terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }
  
}
