ArrayList<Country> COUNTRIES = new ArrayList <Country>();
ArrayList<Terrain> TERRAIN = new ArrayList <Terrain>();
ArrayList<Wheat> WHEAT = new ArrayList <Wheat>();

private int[][] map = new int[100][60];
private int numCountries = 0;
private int numWheat = 1;

public int terrainWheatID = 69; //Declare wheat map id || MAKE SURE THAT THIS NUMBER DOESN'T OVERLAP WITH OTHER IDs



void setup() {
  background(0,0,0);
  noStroke();
  size(1000, 600);

  for (int i = 0; i < numCountries; i++) {
    float xValue = int(random(9))*100;
    float yValue = int(random(5))*100;
    //Add new country by the following parameters: (float positionX, float positionY, String nation, float troops, float growthRate, color flagColor)

    COUNTRIES.add(new Country(xValue, yValue,"blue", color(random(200),random(200), random(200))));
  }

  for (int i = 0; i < numWheat; i++) {
    float xValue = int(random(9))*100;
    float yValue = int(random(5))*100;

    WHEAT.add(new Wheat(xValue, yValue,true, 1, color(249,194,46)));
  }
}

void draw() {
  int countryID = 0; //Declare integer counter for country ID
  for ( Country i: COUNTRIES) {
    countryID += 1;
    i.display();
    i.simulate(this.map, countryID, terrainWheatID);
  }

  for (Wheat i: WHEAT){
    i.display();
    i.simulate(this.map, terrainWheatID);
  }
}
