import java.util.Random;

class Country {
  float positionX;
  float positionY;
  int dmg;
  int dmgDealt;
  int dmgTaken;

  color flagColor;
  String nation;
  public int growthRate;
  int  randomPositionChange;
  int  randomPositionChange2;
  int[] positionChangeArray;
  int counter;
  int[] map;


  Country(float positionX, float positionY, String nation, color flagColor){
    this.positionX = positionX;
    this.positionY = positionY;
    this.nation = nation;
    this.flagColor = flagColor;
    this.positionChangeArray = new int[3];
    this.counter = 0;
  }

 int troops = int(random(10000));
 int population = int(random(10000));
 int food = int(random(100000));

 void checkCoordinateChange(){
    this.positionChangeArray [0] = -10;
    this.positionChangeArray [1] = 10;
    this.positionChangeArray [2] = 0;
    int rnd = new Random().nextInt(positionChangeArray.length);
    int rnd2 = new Random().nextInt(positionChangeArray.length);
    randomPositionChange = positionChangeArray[rnd];
    randomPositionChange2 = positionChangeArray[rnd2];

    if (int(this.positionX + randomPositionChange)/10 <= 1){
      this.positionX += 10;
    }

    if (int(this.positionY + randomPositionChange2)/10 <= 1) {
       this.positionY += 10;
    }

    if (int(this.positionX + randomPositionChange )/10 >= 100) {
       this.positionX -= 10;
    }

    if (int(this.positionY + randomPositionChange2)/10 >= 60) {
       this.positionY -= 10;
    }


 }
 public void expandCountry(int[][] map, int countryID, int terrainWheatID) {
          noStroke();
          fill(this.flagColor);
          circle(this.positionX, this.positionY, 10);
          checkCoordinateChange();

          map[int(this.positionX/10)][int(this.positionY/10)] = countryID;

          if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == 0){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == countryID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] == terrainWheatID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
            food += int(random(100000));
          }

          else if (map[int(this.positionX + randomPositionChange)/10][int(this.positionY + randomPositionChange2)/10] != countryID){
            checkCoordinateChange();
          }
 }

  void display(){
    noStroke();
    fill(this.flagColor);
    circle(this.positionX, this.positionY, 10);
  }

  void simulate(int[][] map, int countryID, int terrainWheatID){

    if (population - food >= 10) {
      this.growthRate = 10;
    }

    else{
      this.growthRate = population - food;
    }

    if (this.counter >= this.growthRate){
      expandCountry(map, countryID, terrainWheatID);

      population += random(100);
      food -= random(100);
      this.counter = 0;
    }

    else {
      this.counter++;
      food -= random(100);

    }


  }
}
