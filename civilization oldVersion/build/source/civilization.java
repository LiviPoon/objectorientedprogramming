import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Random; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class civilization extends PApplet {

ArrayList<Country> COUNTRIES = new ArrayList <Country>();
ArrayList<Terrain> TERRAIN = new ArrayList <Terrain>();
ArrayList<Wheat> WHEAT = new ArrayList <Wheat>();

private int[][] map = new int[100][60];
private int numCountries = 0;
private int numWheat = 1;

public int terrainWheatID = 69; //Declare wheat map id || MAKE SURE THAT THIS NUMBER DOESN'T OVERLAP WITH OTHER IDs



public void setup() {
  background(0,0,0);
  noStroke();
  

  for (int i = 0; i < numCountries; i++) {
    float xValue = PApplet.parseInt(random(9))*100;
    float yValue = PApplet.parseInt(random(5))*100;
    //Add new country by the following parameters: (float positionX, float positionY, String nation, float troops, float growthRate, color flagColor)

    COUNTRIES.add(new Country(xValue, yValue,"blue", color(random(200),random(200), random(200))));
  }

  for (int i = 0; i < numWheat; i++) {
    float xValue = PApplet.parseInt(random(9))*100;
    float yValue = PApplet.parseInt(random(5))*100;

    WHEAT.add(new Wheat(xValue, yValue,true, 1, color(249,194,46)));
  }
}

public void draw() {
  int countryID = 0; //Declare integer counter for country ID
  for ( Country i: COUNTRIES) {
    countryID += 1;
    i.display();
    i.simulate(this.map, countryID, terrainWheatID);
  }

  for (Wheat i: WHEAT){
    i.display();
    i.simulate(this.map, terrainWheatID);
  }
}


class Country {
  float positionX;
  float positionY;
  int dmg;
  int dmgDealt;
  int dmgTaken;

  int flagColor;
  String nation;
  public int growthRate;
  int  randomPositionChange;
  int  randomPositionChange2;
  int[] positionChangeArray;
  int counter;
  int[] map;


  Country(float positionX, float positionY, String nation, int flagColor){
    this.positionX = positionX;
    this.positionY = positionY;
    this.nation = nation;
    this.flagColor = flagColor;
    this.positionChangeArray = new int[3];
    this.counter = 0;
  }

 int troops = PApplet.parseInt(random(10000));
 int population = PApplet.parseInt(random(10000));
 int food = PApplet.parseInt(random(100000));

 public void checkCoordinateChange(){
    this.positionChangeArray [0] = -10;
    this.positionChangeArray [1] = 10;
    this.positionChangeArray [2] = 0;
    int rnd = new Random().nextInt(positionChangeArray.length);
    int rnd2 = new Random().nextInt(positionChangeArray.length);
    randomPositionChange = positionChangeArray[rnd];
    randomPositionChange2 = positionChangeArray[rnd2];

    if (PApplet.parseInt(this.positionX + randomPositionChange)/10 <= 1){
      this.positionX += 10;
    }

    if (PApplet.parseInt(this.positionY + randomPositionChange2)/10 <= 1) {
       this.positionY += 10;
    }

    if (PApplet.parseInt(this.positionX + randomPositionChange )/10 >= 100) {
       this.positionX -= 10;
    }

    if (PApplet.parseInt(this.positionY + randomPositionChange2)/10 >= 60) {
       this.positionY -= 10;
    }


 }
 public void expandCountry(int[][] map, int countryID, int terrainWheatID) {
          noStroke();
          fill(this.flagColor);
          circle(this.positionX, this.positionY, 10);
          checkCoordinateChange();

          map[PApplet.parseInt(this.positionX/10)][PApplet.parseInt(this.positionY/10)] = countryID;

          if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == 0){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == countryID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] == terrainWheatID){
            this.positionX = this.positionX + randomPositionChange;
            this.positionY = this.positionY + randomPositionChange2;
            food += PApplet.parseInt(random(100000));
          }

          else if (map[PApplet.parseInt(this.positionX + randomPositionChange)/10][PApplet.parseInt(this.positionY + randomPositionChange2)/10] != countryID){
            checkCoordinateChange();
          }
 }

  public void display(){
    noStroke();
    fill(this.flagColor);
    circle(this.positionX, this.positionY, 10);
  }

  public void simulate(int[][] map, int countryID, int terrainWheatID){

    if (population - food >= 10) {
      this.growthRate = 10;
    }

    else{
      this.growthRate = population - food;
    }

    if (this.counter >= this.growthRate){
      expandCountry(map, countryID, terrainWheatID);

      population += random(100);
      food -= random(100);
      this.counter = 0;
    }

    else {
      this.counter++;
      food -= random(100);

    }


  }
}

class Terrain {
  private float terrainPositionX;
  private float terrainPositionY;
  private float terrainGrowthRate;
  private int  randomPositionChange;
  private int  randomPositionChange2;
  private int[] positionChangeArray;
  private int terrainColor;
  private int counter;
  private int circleCounter = 0;
  private String statechange = "up";
  public boolean expandableTerrain;




  Terrain(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor) {
    this.expandableTerrain = expandableTerrain;
    this.terrainColor = terrainColor;
    this.terrainPositionX = terrainPositionX;
    this.terrainPositionY = terrainPositionY;
    this.terrainGrowthRate = terrainGrowthRate;
    this.positionChangeArray = new int[3];
  }

  public void display(){
    fill(this.terrainColor);
    circle(this.terrainPositionX,this.terrainPositionY,10);

  };

  private void checkTerrainGrowthCoordinate(int[][] map, int terrainID){
    print(statechange);

    if(statechange == "up" && map[PApplet.parseInt(this.terrainPositionX/10+ 1)][PApplet.parseInt(this.terrainPositionY/10)] != terrainID){
      statechange = "right";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionY++;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };

    if(statechange == "right" && map[PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt((this.terrainPositionY/10) - 1)] != terrainID){
      statechange = "down";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionX++;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };

    if(statechange == "down" && map[PApplet.parseInt(this.terrainPositionX/10 - 1)][PApplet.parseInt(this.terrainPositionY/10)] != terrainID){
      statechange = "left";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionY--;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };

    if(statechange == "left" && map [PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt(this.terrainPositionY/10 + 1)] != terrainID){
      statechange = "up";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionX--;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };


  }

  private void growTerrain(int[][] map, int terrainID){
          circle(this.terrainPositionX,this.terrainPositionY,10);
          fill(this.terrainColor);
          map[PApplet.parseInt(this.terrainPositionX/10)][PApplet.parseInt(this.terrainPositionY/10)] = terrainID;
          checkTerrainGrowthCoordinate(map, terrainID);


  }

  public void simulate(int[][] map, int terrainID){
    if (this.counter >= this.terrainGrowthRate){
      growTerrain(map, terrainID);
      this.counter = 0;
    }
    else {
      this.counter++;
    }


  }
}
class Wheat extends Terrain {
  
  Wheat(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, int terrainColor){
    super(terrainPositionX, terrainPositionY, expandableTerrain, terrainGrowthRate, terrainColor);
  }
  
}
  public void settings() {  size(1000, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "civilization" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
