
class Terrain {
  private float terrainPositionX;
  private float terrainPositionY;
  private float terrainGrowthRate;
  private int  randomPositionChange;
  private int  randomPositionChange2;
  private int[] positionChangeArray;
  private color terrainColor;
  private int counter;
  private int circleCounter = 0;
  private String statechange = "up";
  public boolean expandableTerrain;




  Terrain(float terrainPositionX, float terrainPositionY, boolean expandableTerrain, float terrainGrowthRate, color terrainColor) {
    this.expandableTerrain = expandableTerrain;
    this.terrainColor = terrainColor;
    this.terrainPositionX = terrainPositionX;
    this.terrainPositionY = terrainPositionY;
    this.terrainGrowthRate = terrainGrowthRate;
    this.positionChangeArray = new int[3];
  }

  public void display(){
    fill(this.terrainColor);
    circle(this.terrainPositionX,this.terrainPositionY,10);

  };

  private void checkTerrainGrowthCoordinate(int[][] map, int terrainID){
    print(statechange);

    if(statechange == "up" && map[int(this.terrainPositionX/10+ 1)][int(this.terrainPositionY/10)] != terrainID){
      statechange = "right";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionY++;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };

    if(statechange == "right" && map[int(this.terrainPositionX/10)][int((this.terrainPositionY/10) - 1)] != terrainID){
      statechange = "down";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionX++;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };

    if(statechange == "down" && map[int(this.terrainPositionX/10 - 1)][int(this.terrainPositionY/10)] != terrainID){
      statechange = "left";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionY--;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };

    if(statechange == "left" && map [int(this.terrainPositionX/10)][int(this.terrainPositionY/10 + 1)] != terrainID){
      statechange = "up";
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    }
    else{
      this.terrainPositionX--;
      circle(this.terrainPositionX, this.terrainPositionY, 10);
    };


  }

  private void growTerrain(int[][] map, int terrainID){
          circle(this.terrainPositionX,this.terrainPositionY,10);
          fill(this.terrainColor);
          map[int(this.terrainPositionX/10)][int(this.terrainPositionY/10)] = terrainID;
          checkTerrainGrowthCoordinate(map, terrainID);


  }

  public void simulate(int[][] map, int terrainID){
    if (this.counter >= this.terrainGrowthRate){
      growTerrain(map, terrainID);
      this.counter = 0;
    }
    else {
      this.counter++;
    }


  }
}
