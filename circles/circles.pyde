import random

game_objects = []

class Thingy(object):
    def __init__(self, x, y):
        '''Create a new circle at the given x,y point with a random speed,
           color, and size.'''
        self.x = x
        self.y = y
        self.x_speed = random.randint(-5,5)
        self.y_speed = random.randint(-5,5)
        self.color = color(random.randint(0,255),
        random.randint(0,255),
        random.randint(0,255))
        self.size = random.randint(5,75)

    def update(self):
        '''Update current location by speed.'''
        self.x += self.x_speed
        self.y += self.y_speed
        
        if self.x > 400:
            self.x_speed = -self.x_speed
    
        if self.x < 0:
            self.x_speed = -self.x_speed
            
        if self.y > 400:
            self.y_speed = -self.y_speed
    
        if self.y < 0:
            self.y_speed = -self.y_speed


    def draw(self):
        '''Prepare to draw self on the canvas.'''
        fill(self.color) # set shape color
        stroke(0,0,0) # add a black outline
        
class Circle(Thingy):
    def draw(self):
        super(Circle, self).draw
        ellipse(self.x, self.y, self.size, self.size)
        
        
class Square(Thingy):
    def draw(self):
        super(Square, self).draw
        square(self.x, self.y, self.size)
        
        


def addCircle():
    '''Add a new circle where the user clicked.'''
    global game_objects
    game_objects.append(Circle(mouseX, mouseY))
    
def addSquare():
    global game_objects
    game_objects.append(Square(mouseX, mouseY))
    
def reset():
    '''Clear all game objects.'''

    global game_objects
    game_objects = []


def setup():
    size(400, 400)

            
        
def draw():
    '''Clear the screen, have all game objects update and redraw.'''
    global game_objects
    i = 0
    
    background(255, 255, 255) # draw a white background
    allInstanceCoordanates = []
    
    for game_object in game_objects:
        game_object.update()
        game_object.draw()
    
    for i in range(len(game_objects)):
        selectedID = None
        
        selectedInstanceCoordanates = game_objects[i].__dict__.values() #grab all the variable values of all the instances
        allInstanceCoordanates.append([selectedInstanceCoordanates[4], selectedInstanceCoordanates[5]])
        
        for elem in allInstanceCoordanates:
            if allInstanceCoordanates.count(elem) > 1:
                selectedID = True
            else:
                selectedID = False
                
        if selectedID == True:
            print("hi")
        
def mouseClicked():
    global game_objects
    addCircle()
    addSquare()



def keyPressed():
    if key == 'r':
        reset()
